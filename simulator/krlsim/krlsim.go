package krlsim

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"time"

	"gitlab.com/yursan9/simultrans/geo"
	"gitlab.com/yursan9/simultrans/simulator"
)

func init() {
	simulator.Register("krl", &KRLSimulator{})
}

type KRL struct {
	Id    string
	Lat   float64
	Long  float64
	Speed float64
	Route string
	From string
	To string
	Station string
}

func (k *KRL) ToJSON() []byte {
	data, err := json.Marshal(k)
	if err != nil {
		log.Fatal("Error converting data: ", err)
	}

	return data
}

type KRLSimulator struct {
	Route         simulator.Path
	Schedules     simulator.TimeTable
	KRL           KRL
	MaxSpeed      float64
	Index         int
	ScheduleIndex int
}

func (sim *KRLSimulator) Prepare(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &sim)
	if err != nil {
		return err
	}
	
	sim.ScheduleIndex = -1
	sim.Index = -1

	return nil
}

func (sim *KRLSimulator) Generate(duration int) ([]byte, bool) {
	now := time.Now()

	if sim.ScheduleIndex == -1 {
		sim.ScheduleIndex = sim.Schedules.Find(now)

		if sim.ScheduleIndex == -1 {
			return nil, false
		}
	}

	sch := sim.Schedules[sim.ScheduleIndex]
	sim.KRL.Id = sch.Id
	sim.KRL.Route = sch.Route
	sim.KRL.From = sch.From
	sim.KRL.To = sch.To
	sim.KRL.Station = sch.Current

	if sim.Index == -1 {
		sim.Index = sim.Route.Find(sim.KRL.Station)

		if sim.Index == -1 {
			return nil, false
		}
	}

	start := sim.Route[sim.Index]
	end := sim.Route[sim.Index+1]

	if sim.KRL.Lat == 0.0 && sim.KRL.Long == 0.0 {
		sim.KRL.Lat = start.Lat
		sim.KRL.Long = start.Long
	}

	distance := geo.Distance(sim.KRL.Lat, sim.KRL.Long, end.Lat, end.Long)
	distance = geo.ToDistanceKm(distance)

	pause := false
	if distance > 0.01 {
		acc := 4.0 * float64(duration)
		if end.Pause {
			sim.KRL.Speed -= acc
			sim.KRL.Speed = clamp(sim.KRL.Speed, 10.0, sim.MaxSpeed)
		} else {
			sim.KRL.Speed += acc
			sim.KRL.Speed = clamp(sim.KRL.Speed, 0.0, sim.MaxSpeed)
		}

		d := geo.ToDistanceRadian(convertKmhToKms(sim.KRL.Speed))
		tc := geo.InitialCourse(sim.KRL.Lat, sim.KRL.Long, end.Lat, end.Long)

		lat, long := geo.GoTo(sim.KRL.Lat, sim.KRL.Long, tc, d)

		sim.KRL.Lat = geo.ToDegree(lat)
		sim.KRL.Long = geo.ToDegree(long)
	} else {
		sim.Index += 1
		sim.KRL.Lat = end.Lat
		sim.KRL.Long = end.Long

		if end.Pause {
			sim.KRL.Speed = 0.0
			sim.KRL.Station = end.Stop
			sim.ScheduleIndex += 1
			sim.Index = -1
			pause = true
		}
	}

	return sim.KRL.ToJSON(), pause
}

func (sim *KRLSimulator) IsFinish() bool {
	return sim.Index == len(sim.Route)-1
}

func (sim *KRLSimulator) Reverse() {
	sim.Route.Reverse()
}


func clamp(n, min, max float64) float64 {
	switch {
	case n > max:
		return max
	case n < min:
		return min
	default:
		return n
	}
}

func convertKmhToKms(n float64) float64 {
	return n * 0.00027778
}

func convertKmsToKmh(n float64) float64 {
	return n * 3600.0
}
