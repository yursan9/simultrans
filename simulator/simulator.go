package simulator

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/yursan9/simultrans/simulator/driver"
)

var (
	driversMu sync.RWMutex
	drivers   = make(map[string]driver.Driver)
)

func Register(name string, driver driver.Driver) {
	driversMu.Lock()
	defer driversMu.Unlock()

	if driver == nil {
		panic("simulator: Register driver is nil")
	}

	if _, dup := drivers[name]; dup {
		panic("simulator: Register called twice for driver " + name)
	}

	drivers[name] = driver
}

type Simulator struct {
	Driver driver.Driver
}

func Prepare(driverName, filename string) (*Simulator, error) {
	driversMu.RLock()
	driveri, ok := drivers[driverName]
	driversMu.RUnlock()
	if !ok {
		return nil, fmt.Errorf("simulator: unknown driver %q (forgotten import?)", driverName)
	}

	err := driveri.Prepare(filename)
	if err != nil {
		return nil, err
	}

	return &Simulator{Driver: driveri}, nil
}

func (s *Simulator) Run(ch chan []byte, reverse, loop bool) {
	if reverse {
		reverser, ok := s.Driver.(driver.Reverser)
		if !ok {
			panic("simulator: Driver does not implement driver.Reverser")
		}

		reverser.Reverse()
	}

	generater, ok := s.Driver.(driver.Generater)
	if !ok {
		panic("simulator: Driver does not implement driver.Generater")
	}

	for {
		data, pause := generater.Generate(3)
		
		if data != nil {
			ch <- data
		}

		if generater.IsFinish() {
			if !loop {
				close(ch)
				break
			}

			reverser, ok := s.Driver.(driver.Reverser)
			if !ok {
				panic("simulator: Driver does not implement driver.Reverser")
			}

			reverser.Reverse()
		}
		if pause {
			time.Sleep(60 * time.Second)
		} else {
			time.Sleep(3 * time.Second)
		}
	}
}
