package driver

type Generater interface {
	Generate(int) ([]byte, bool)
	IsFinish() bool
}

type Driver interface {
	Prepare(string) error
}

type Reverser interface {
	Reverse()
}
