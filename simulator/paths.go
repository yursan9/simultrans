package simulator

// Point is coordinate and event on that coordinate
type Point struct {
	Lat   float64
	Long  float64
	Pause bool
	Stop  string
}

// Path is a path made by slice of point
type Path []Point

// Reverse reversed the position of Point
func (p Path) Reverse() {
	for i, j := 0, len(p)-1; i < j; i, j = i+1, j-1 {
		p[i], p[j] = p[j], p[i]
	}
}

func (p Path) Find(stop string) int {
	for i, v := range p {
		if v.Stop == stop {
			return i
		}
	}

	return -1
}
