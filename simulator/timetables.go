package simulator

import (
	"encoding/json"
	"time"
)

type Schedule struct {
	Id        string
	Route     string
	From      string
	To        string
	Current   string
	Arrival   time.Time
	Departure time.Time
}

func (s *Schedule) MarshalJSON() ([]byte, error) {
	type Alias Schedule
	layout := "15:04:05"
	return json.Marshal(&struct {
		Arrival string
		*Alias
	}{
		Arrival: s.Arrival.Format(layout),
		Alias:   (*Alias)(s),
	})
}

func (s *Schedule) UnmarshalJSON(data []byte) error {
	type Alias Schedule
	aux := &struct {
		Arrival   string
		Departure string
		*Alias
	}{
		Alias: (*Alias)(s),
	}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	layout := "15:04:05"
	arr, err := time.Parse(layout, aux.Arrival)
	if err != nil {
		return err
	}
	dep, err := time.Parse(layout, aux.Departure)
	if err != nil {
		return err
	}

	now := time.Now()
	arr = time.Date(now.Year(), now.Month(), now.Day(), arr.Hour(), arr.Minute(), arr.Second(), arr.Nanosecond(), now.Location())
	dep = time.Date(now.Year(), now.Month(), now.Day(), dep.Hour(), dep.Minute(), dep.Second(), dep.Nanosecond(), now.Location())

	s.Arrival = arr
	s.Departure = dep
	return nil
}

type TimeTable []Schedule

func (tt TimeTable) Find(now time.Time) int {
	for i, v := range tt {
		if now.After(v.Arrival) && now.Before(v.Departure) {
			return i
		}
	}

	return -1
}
