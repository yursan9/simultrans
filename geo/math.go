package geo

import "math"

// ToRadian converts n to radians
func ToRadian(n float64) float64 {
	return n * math.Pi / 180.0
}

// ToDegree converts n to degree
func ToDegree(n float64) float64 {
	return n * 180.0 / math.Pi
}

// ToKilometer converts angular units to kilometer
func ToDistanceKm(n float64) float64 {
	return n * EarthRadius
}

func ToDistanceRadian(n float64) float64 {
	return n / EarthRadius
}
