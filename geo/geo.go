package geo

import "math"

const (
	EarthRadius = 6378.1 // km
)

// Distance calculate the distance between 2 coordinate using haversine formula
// return distance in radians.
// http://www.edwilliams.org/avform.htm#Dist
func Distance(lat1, long1, lat2, long2 float64) float64 {
	lat1 = ToRadian(lat1)
	long1 = ToRadian(long1)
	lat2 = ToRadian(lat2)
	long2 = ToRadian(long2)

	dlong := long2 - long1
	a := math.Sin(lat1)*math.Sin(lat2) + math.Cos(lat1)*math.Cos(lat2)*math.Cos(dlong)
	return math.Acos(a)
}

// InitialCourse calculate heading direction when going between 2 coordinate
// return direction in radians
// http://www.movable-type.co.uk/scripts/latlong.html
func InitialCourse(lat1, long1, lat2, long2 float64) float64 {
	lat1 = ToRadian(lat1)
	long1 = ToRadian(long1)
	lat2 = ToRadian(lat2)
	long2 = ToRadian(long2)

	dlong := long2 - long1
	y := math.Sin(dlong) * math.Cos(lat2)
	x := math.Cos(lat1)*math.Sin(lat2) - math.Sin(lat1)*math.Cos(lat2)*math.Cos(dlong)

	return math.Atan2(y, x)
}

// GoTo calculate coordinate given initial coordinate with direction and distance
// return latitude and longitude in radians
// http://www.movable-type.co.uk/scripts/latlong.html
func GoTo(lat, long float64, course float64, distance float64) (float64, float64) {
	lat = ToRadian(lat)
	long = ToRadian(long)

	atLat := math.Asin(math.Sin(lat)*math.Cos(distance) + math.Cos(lat)*math.Sin(distance)*math.Cos(course))
	atLong := long + math.Atan2(math.Sin(course)*math.Sin(distance)*math.Cos(lat),
		math.Cos(distance)-math.Sin(lat)*math.Sin(atLat))

	return atLat, atLong
}

// Intermediate calculate coordinate between 2 coordinate given fraction of number
// return latitude and longitude in radians.
// Inspired by : http://www.edwilliams.org/avform.htm#Intermediate
func Intermediate(lat1, long1, lat2, long2, f float64) (float64, float64) {
	d := Distance(lat1, long1, lat2, long2)

	A := math.Sin((1.0-f)*d) / math.Sin(d)
	B := math.Sin(f*d) / math.Sin(d)

	lat1 = ToRadian(lat1)
	long1 = ToRadian(long1)
	lat2 = ToRadian(lat2)
	long2 = ToRadian(long2)

	x := A*math.Cos(lat1)*math.Cos(long1) + B*math.Cos(lat2)*math.Cos(long2)
	y := A*math.Cos(lat1)*math.Sin(long1) + B*math.Cos(lat2)*math.Sin(long2)
	z := A*math.Sin(lat1) + B*math.Sin(lat2)

	lat := math.Atan2(z, math.Sqrt(math.Pow(x, 2)+math.Pow(y, 2)))
	long := math.Atan2(y, x)

	return lat, long
}
