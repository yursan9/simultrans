package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/yursan9/simultrans/sender"
	"gitlab.com/yursan9/simultrans/simulator"
	_ "gitlab.com/yursan9/simultrans/simulator/krlsim"
)

const (
	DriverName = "krl"
)

var (
	SimFile     string
	ReceiverUrl string
	ApiKey      string

	LoopMode    bool
	ReverseMode bool
)

func init() {
	SimFile = os.Getenv("SIMULTRANS_FILE")
	ReceiverUrl = os.Getenv("SIMULTRANS_URL")
	ApiKey = os.Getenv("SIMULTRANS_KEY")

	// Set command-line flag rules
	flag.BoolVar(&LoopMode, "loop", false, "Loop the simulation")
	flag.BoolVar(&ReverseMode, "reverse", false, "Reverse the simulation path")

	flag.Parse()
}

func main() {
	sim, err := simulator.Prepare(DriverName, SimFile)
	if err != nil {
		log.Fatal(err)
	}

	sn := sender.New(ReceiverUrl, ApiKey)

	ch := make(chan []byte)
	go sim.Run(ch, ReverseMode, LoopMode)
	for {
		data, ok := <-ch
		if !ok {
			log.Fatal("Channel closed")
		}
		fmt.Printf("%s\n", data)
		err := sn.Send(data)
		if err != nil {
			log.Fatal(err)
		}
	}
}
