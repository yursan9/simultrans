package sender

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const (
	ContentType = "application/json"
)

type Sender interface {
	Send([]byte) error
}

type HttpSender struct {
	Client *http.Client
	Req    *http.Request
}

func New(url, key string) *HttpSender {
	client := &http.Client{
		Timeout: 3 * time.Second,
	}

	req, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		log.Println("Error preparing new request: ", err)
		return nil
	}
	req.Header.Set("Content-Type", ContentType)
	req.Header.Set("Authorization", "ApiKey "+key)

	return &HttpSender{
		Client: client,
		Req:    req,
	}
}

func (s *HttpSender) Send(data []byte) error {
	s.Req.Body = ioutil.NopCloser(bytes.NewReader(data))
	resp, err := s.Client.Do(s.Req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return &ResponseError{Status: resp.Status}
	}

	return nil
}

type ResponseError struct {
	Status string
}

func (re *ResponseError) Error() string {
	return fmt.Sprintf("HTTP error got code: %s", re.Status)
}
